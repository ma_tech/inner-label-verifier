import { StatusBar } from 'expo-status-bar';
import { useState,useEffect, Component } from 'react';
import { StyleSheet, View, Alert, Image, TouchableOpacity, ScrollView, BackHandler, Pressable,TouchableWithoutFeedback } from 'react-native';
import { SimpleAccordion } from 'react-native-simple-accordion';
import FabGroup from './FabGroup';
import ConfirmationModal from './Modal';
import { useTheme, Button, Surface, Text, TextInput, Dialog, Portal } from 'react-native-paper';
import { Audio } from 'expo-av';
import Spinner from 'react-native-loading-spinner-overlay';


export default function QRdetails({route, navigation}) {
  const [loadChecker, setLoadChecker] = useState(global.firstLoadChecker.load)
    const buzzingSound = require('../assets/sounds/mismatch.mp3');
    const [sound, setSound] = useState();
    const [fetchData,setFetchData] = useState(global.qrData);
    const [boxData, setBoxData] = useState([])
    const [lotNumber, setLotNumber] = useState('')
    const [modalCaller, setModalCaller] = useState(false)
    const [uploadModal, setUploadModal] = useState(false)
    const [comparison, setComparison] = useState('')
    const [visible, setVisible] = useState(true);
    const [match, setMatch] = useState(false)
    const [resultContainerModal, setResultContainerModal] = useState([])
    const [multipleResultVisible, setMultipleResultVisible] = useState(true)
    const showDialog = () => setVisible(true);

    const hideDialog = () => setVisible(false);

    const hideResult = () => setMultipleResultVisible(false);
    // const [inputBtn, setInpuptBtn] = useState(false)
    // const [keyboardState,setKeyboardState] = useState(true)
    // const [qrData, setQRData] = useState([route.params])
    // setFetchData(route.params.resultData[0])
  // console.log(route.params.data)
  // console.log("tester")
    const testFetch = ()=>{
      
    }
    // console.log(global.boxAdditionalData)
    // console.log(global.qrData[1])

    // useEffect(()=>{
    //   // setFetchData(global.qrData)
    //   // console.log(fetchData[1])
    // },[global.qrData])

    useEffect(()=>{
      if(route.params !== undefined){
        if(route.params.modalType === "cancel"){
          setModalCaller(true)
          setUploadModal(false)
        }else if(route.params.modalType === "upload"){
          setVisible(true)
          setUploadModal(true)
          setModalCaller(false)
        }else if(route.params.reelScan === true){
          console.log("once")
          if(loadChecker === true){
            // console.log(loadChecker)
            // setMultipleResultVisible(true) 
            setResultContainerModal(
                <Portal>
                  <Dialog visible={multipleResultVisible} dismissable={false} style={{ backgroundColor: '#fff' }}>
                    <Dialog.Title>Select 1 goods code:</Dialog.Title>
                    <ScrollView style={{width:'100%'}} keyboardShouldPersistTaps="handled">
                    <Dialog.Content>
                      {global.qrData.map((result, index) => {
                        if (index === global.resultLength) {
                          return null; // or any other logic you want to handle the last iteration
                        }
                        return (
                            <TouchableWithoutFeedback key={index} onPress={() => setView(result)} >
                              <View style={{ shadowColor: '#000', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.2, shadowRadius: 4,backgroundColor:'#fff', elevation: 5, width:'90%', height:50, alignSelf:'center',padding:8,paddingLeft: 15,borderRadius:5,justifyContent:'center',marginBottom:10 }}>
                                <Text variant="bodyMedium">{result.GOODS_CODE + ' - ' + result.ASSY_LINE}</Text>
                              </View>
                          </TouchableWithoutFeedback>
                          )
                      })}
                    </Dialog.Content>
                    </ScrollView>
                  </Dialog>
                </Portal>
              
            );
          }else{
            global.firstLoad = global.qrData[0]
            setMultipleResultVisible(false) 
          }
        }
      }
      
    },[route.params])

    useEffect(()=>{
      setLotNumber(fetchData[1].LOT_NUMBER)
      // console.log(fetchData)
    },[fetchData[1]])
    // disable hardware back button
    useEffect(() => {
        const backHandler = BackHandler.addEventListener('hardwareBackPress', () => true)
        return () => backHandler.remove()
      }, [])

      useEffect(() => {
        // console.log(loadChecker)
        
        // console.log(global.firstLoad)
          console.log("render once")
        
      },[loadChecker])

      // const resultContainerModal = (
      //   <Portal>
      //     <Dialog visible={multipleResultVisible} dismissable={false}>
      //     <Dialog.Title>Alert</Dialog.Title>
      //     <Dialog.Content>
      //       <Text variant="bodyMedium">This is simple dialog</Text>
      //       <Text variant="bodyMedium">This is simple dialog</Text>
      //     </Dialog.Content>
      //     <Dialog.Actions>
      //       <Button onPress={hideResult}>Done</Button>
      //     </Dialog.Actions>
      //   </Dialog>
      //   </Portal>
      //   )


let setView = (data) => {
  console.log(data)
  setMultipleResultVisible(false) 
  global.firstLoadChecker.load = false
  // console.log(data)
  global.firstLoad = data
}

    const view = (
        <View>
            <View style={{paddingTop:7,paddingBottom:7}}>
                <Text variant="labelMedium" style={{alignSelf:'flex-start', color:'#3C5393'}}>ASSY LINE:</Text>
                 <Text variant="titleMedium" style={{alignSelf:'flex-start'}}>{global.firstLoad.ASSY_LINE}</Text>
             </View>
             <View style={{paddingTop:7,paddingBottom:7}}>
                <Text variant="labelMedium" style={{alignSelf:'flex-start', color:'#3C5393'}}>ITEM CODE:</Text>
                 <Text variant="titleMedium" style={{alignSelf:'flex-start'}}>{global.firstLoad.ITEM_CODE}</Text>
             </View>
             <View style={{paddingTop:7,paddingBottom:7}}>
                <Text variant="labelMedium" style={{alignSelf:'flex-start', color:'#3C5393'}}>PART NUMBER:</Text>
                 <Text variant="titleMedium" style={{alignSelf:'flex-start'}}>{global.firstLoad.PART_NUMBER}</Text>
             </View>
             <View style={{paddingTop:7,paddingBottom:7}}>
                <Text variant="labelMedium" style={{alignSelf:'flex-start', color:'#3C5393'}}>PART NAME:</Text>
                 <Text variant="titleMedium" style={{alignSelf:'flex-start'}}>{global.firstLoad.PART_NAME}</Text>
             </View>
             <View style={{paddingTop:7,paddingBottom:7}}>
                 <Text variant="labelMedium" style={{alignSelf:'flex-start', color:'#3C5393'}}>MANUFACTURER NAME:</Text>
                 <Text variant="titleMedium" style={{alignSelf:'flex-start'}}>{global.firstLoad.MANUFACTURER_NAME}</Text>
             </View>
             <View style={{paddingTop:7,paddingBottom:7}}>
                <Text variant="labelMedium" style={{alignSelf:'flex-start', color:'#3C5393'}}>UPDATED PRODUCT NAME:</Text>
                 <Text variant="titleMedium" style={{alignSelf:'flex-start'}}>{global.firstLoad.UPDATED_PRODUCT_NAME}</Text>
             </View>
        </View>
    )
   const accordionData = (
    <View style={{width:'100%'}}>  
      <SimpleAccordion 
        viewInside={view} 
        title={global.firstLoad.GOODS_CODE}
        titleStyle={{alignSelf:'flex-start'}}

        bannerStyle={{
          borderRadius:5,
            shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        backgroundColor:"#fff",
        elevation: 5,
        width:'90%',
        alignSelf:'center'
        }}

        viewContainerStyle={{
            padding:8,
            paddingLeft: 20,
            // alignItems:'center',
            width: '90%',
            alignSelf:'center',
            
        }}
      />
    </View>  
)



useEffect(()=>{
  
  if (global.boxAdditionalData.data){
    // console.log(global.boxAdditionalData.data)
    // let x = 0
    setBoxData(()=>{
      return(
          
        <View style={{ shadowColor: '#000', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.2, shadowRadius: 4,backgroundColor:"#fff", elevation: 5, width:'90%', height:60, alignSelf:'center',padding:8,paddingLeft: 15,borderRadius:5,justifyContent:'center' }}>
          <Text variant='titleMedium' style={{fontWeight:'bold',}}>{global.boxAdditionalData.data}</Text>
        </View>

      )
    }
    )
    
    if(global.firstLoad.GOODS_CODE == global.boxAdditionalData.data){
      setComparison(()=>{
        setMatch(true)
        global.matchScanned = true
        return(<Text variant='displayMedium' style={{alignSelf:'center', fontWeight:'bold', color:'green'}}>Matched</Text>)
      })
    }else {
      playSound();
      setComparison(()=>{
        setMatch(false)
        global.matchScanned = false
        return(<Text variant='displayMedium' style={{alignSelf:'center', fontWeight:'bold', color:'red'}}>Don't Match</Text>)
      })
    }
    
  }
},[global.boxAdditionalData.data])

async function playSound() {
  // console.log('Loading Sound');
  const { sound } = await Audio.Sound.createAsync(buzzingSound);
  setSound(sound);

  // console.log('Playing Sound');
  await sound.playAsync();
}

// Function to stop the sound
async function stopSound() {
  if (sound) {
    await sound.stopAsync();
    // Release the sound object
    sound.unloadAsync();
    setSound(null);
  }
}

// Function to handle the button press
const handleButtonPress = async () => {
  await stopSound();
  navigation.navigate('Scanner', { method: 'supplier-scan' });
};

const lotNumberChange = (text)=> {
  // console.log(text)
  // console.log(global.qrData[1].LOT_NUMBER)
  fetchData[1].LOT_NUMBER = text
  global.qrData[1].LOT_NUMBER = text
  setLotNumber(text); 
  // console.log(fetchData)
  setModalCaller(false)
  setUploadModal(false)
}

const lotNumberVerifier = (
  <Portal>
      <Dialog visible={visible} onDismiss={hideDialog}>
      <Dialog.Icon icon="alert" color='red' />
      <Dialog.Title style={{textAlign: 'center'}}>STOP</Dialog.Title>
        <Dialog.Content>
            <Text style={{alignSelf:'center'}} variant="bodyMedium">Goods Code mismatch!</Text>
        </Dialog.Content>
        <Dialog.Actions>
          <Button labelStyle={{color:'black'}} onPress={hideDialog}>OK</Button>
        </Dialog.Actions>
      </Dialog>
    </Portal>
)
    return (
      <View style={styles.container}>
        <Surface style={styles.surface}>
          <View style={{paddingLeft: '3%', alignSelf: 'flex-start', height:'50%', width:'18%'}}>
              <Image
              source={require('../assets/matech_logo_4.png')}
              style={{height:'100%', width:'100%'}}
              />
          </View>
        </Surface>
        <Text variant='titleLarge' style={{alignSelf:'flex-start',paddingLeft:'5%', fontWeight:'bold', color:'#3C5393'}}>Item Code/Part Number</Text>
        <ScrollView style={{width:'100%'}} keyboardShouldPersistTaps="handled">
            <View style={styles.separator}/>
            {
                accordionData
            }
            <View style={styles.separator}/>
            {global.boxAdditionalData.data ? 
            <>
              <Text variant='titleLarge' style={{alignSelf:'flex-start',paddingLeft:'5%', fontWeight:'bold', color:'#3C5393'}}>MAT Label QR</Text>
              <View style={styles.separator}/>
              {boxData}
              <View style={styles.separator}/>
              </>  
            : null}
            
            {comparison}
            {match === true ?
              <TextInput
              maxLength={50}
              multiline={true}
              numberOfLines={1}
              editable={true}
              value={lotNumber}
              style={{width:'90%', alignSelf:'center', backgroundColor:'#FFFFFF', marginTop:30}}
              mode="outlined"
              label="Lot Number"
              placeholder="Input lot number"
              right={<TextInput.Icon icon="qrcode-scan" iconColor='#3C5393' onPress={()=>navigation.navigate('Scanner', {'method':'lot-scan'})}/>}
              theme={{colors: {primary: '#414141', underlineColor: 'transparent'}}}
              onChangeText={lotNumberChange}
              // left={<TextInput.Icon icon="square-edit-outline" onPress={scanLotNumber}/>}
            />
            : null}
      
        {modalCaller ? <ConfirmationModal open={true} operation="restart your progress" content="content"/> : ""}
        {uploadModal ? (global.matchScanned === true && Object.keys(global.boxAdditionalData).length !== 0) ? <ConfirmationModal open={true} operation="upload"/>: lotNumberVerifier : ""}
        {/* {lotNumberVerifier} */}
        {multipleResultVisible ? resultContainerModal : ""}
        </ScrollView>
        {/* <SimpleAccordion viewInside={<View/>} title={"My Accordion Title"}/> */}
        <FabGroup />
        <StatusBar style="dark" />
        
        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
          <Button 
            style={{
              width: '50%',
              alignSelf: 'center',
              // marginBottom: 20,
              position: 'absolute',
              bottom: 20,
            }}
            icon="qrcode-scan"
            mode="elevated"
            buttonColor="#3C5393"
            contentStyle={{ height: 50, width: 180, alignSelf: 'center' }}
            labelStyle={{ fontSize: 16, fontWeight: 'bold' }}
            textColor="white"
            onPress={handleButtonPress}
          >
            MAT Code
          </Button>
        </View>
      </View>
      
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      
      // justifyContent: 'center',
    },
    exampleBox: {
        marginVertical: 16,
        paddingHorizontal: 16
      },
      separator: {
        height: 1,
        backgroundColor: "#000000",
        marginVertical: 16,
        width:'90%',
        alignSelf:'center'
      },
    addBtn:{
        flex:1,
        alignItems:'center',
        border:1
    },
    surface: {
        padding: 8,
        height: '10%',
        width: "100%",
        alignItems: 'flex-start',
        justifyContent:'center',
        marginTop:24, 
        marginBottom:15,
        backgroundColor:'#3C5393'
      },
  });
