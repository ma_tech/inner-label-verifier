import { StatusBar } from 'expo-status-bar';
import { useState,useCallback, useEffect } from 'react';
import { StyleSheet, View, TextInput, Alert, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import QRIcon from 'react-native-vector-icons/MaterialIcons'
import { useTheme, Button, Surface, IconButton, MD3Colors , Portal, Dialog, Text } from 'react-native-paper';
import { useFonts } from 'expo-font';
import * as SplashScreen from 'expo-splash-screen';
import Constants from 'expo-constants';
import * as Linking from 'expo-linking';


SplashScreen.preventAutoHideAsync();

export default function HomeScreen({route,navigation}) {
  const myIcon = <Icon name="wifi" size={130} color="#3C5393" />;
  const qrIcon = <QRIcon name="qr-code-scanner" size={180} color="gray" style={{marginTop:'20%'}}/>
  const [name,setName] = useState('');
  const theme = useTheme();
  const [userNameContainer, setUserNameContainer] = useState('');
  const [passwordNameContainer, setPasswordContainer] = useState('');
  const [dialogVisible, setDialogVisible] = useState(false);
    
  const [fontsLoaded] = useFonts({
    'Roboto-Medium': require('../assets/fonts/Roboto-Medium.ttf'),
  });

const version = Constants.manifest.version;
// console.log('App version:', version);

useEffect(() => {
  fetch(`${global.backend}/api/check-version.php`,{
    method: 'POST',
    headers:{
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      'secretKey': global.secretKey,
    })
  })
    .then(response => response.json())
    .then((result) => {
      console.log(result[0].VERSION)
      const str = result[0].VERSION;
      const trimmedStr = str.replace(/\s/g, '');
      if(trimmedStr !== version){
        global.appURL = result[0]
        console.log(global.appURL)
        Alert.alert(
          "Update Available",
          "Please update the app to the latest version",
          [
            {
             
              style: "cancel"
            },
            { text: "Update", onPress: () => Linking.openURL(result[0].URL) }
          ]
        );
      }
    })
},[])

  useEffect(()=>{
    if(route.params !== undefined){
      if(route.params.process === "login"){
        setUserNameContainer(global.credentials[0].EMP_NUM)
        setPasswordContainer(global.credentials[0].password)
      }
    }
    // console.log(route.params)
  },[route.params])
  const onLayoutRootView = useCallback(async () => {
    if (fontsLoaded) {
      await SplashScreen.hideAsync();
    }
  }, [fontsLoaded]);

  if (!fontsLoaded) {
    return null;
  }

  const showDialog = () => setDialogVisible(true);

  const logout = () => {
      (async () => {
        global.credentials = await ""
        // global.password = await ""
        await setUserNameContainer('')
        await setPasswordContainer('')
        setDialogVisible(true)
    })();
  }
  return (
    <View style={{backgroundColor:theme.colors.surface,flex:1,alignItems:'center'}} onLayout={onLayoutRootView}>
        <Surface style={styles.surface}>
          {
            userNameContainer == "" && passwordNameContainer == "" ?
            null
            :
            <View style={styles.buttonContainer}>
              <IconButton
                icon="power"
                iconColor={'#3C5393'}
                size={40}
                onPress={logout}
              />
            </View>
          }
        
          <Image
            source={require('../assets/matech_logo_4.png')}
            style={{height:'40%', width:'35%',marginTop:20}}
          />
          <Text style={{fontSize:25, fontWeight:'bold',fontFamily:'Roboto-Medium', lineHeight:40, color:'#fff'}}>Inner Label Verifier</Text>
        <Text style={{fontSize:25, fontWeight:'bold',fontFamily:'Roboto-Medium',marginBottom:15, color:'#fff'}}>QR Scanner</Text>
        </Surface>
        
        <Portal>
            <Dialog visible={dialogVisible} dismissable={true} onDismiss={() => setDialogVisible(false)} style={{ backgroundColor: '#fff' }}>
            {/* <Dialog.Title>Login Failed</Dialog.Title> */}
            <Dialog.Content>
            <Text variant="titleMedium">Thank you!</Text>
            </Dialog.Content>
                <Dialog.Actions style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                  <Button onPress={() => setDialogVisible(false)}>close</Button>
                </Dialog.Actions>
            </Dialog>
        </Portal>
        {/* {myIcon} */}
        {qrIcon}
        {/* <TextInput style={{backgroundColor:'red', width:300,height:50,borderRadius:10}} */}
        {/* onChangeText={setName} */}
        {/* /> */}
      {/* <Text>Hello Sherwin</Text> */}
      {/* <Text
      elevation={5}
        style={styles.button}
        onPress = {()=> navigation.navigate('Scanner')}
      >
        <Text>GET STARTED</Text>
      </Text> */}
      {
        userNameContainer == "" && passwordNameContainer == ""
        ?
      <Button 
        style={{marginTop:100}} 
        icon="account-key" 
        mode="elevated" 
        buttonColor='#3C5393' 
        contentStyle={{height:70,width:220}} 
        labelStyle={{fontSize:20, fontWeight:'bold'}} 
        textColor="white" 
        uppercase="true"
        onPress = {()=> navigation.navigate('Login')}
      >Log-in</Button> 
      :
      <Button 
        style={{marginTop:100}} 
        icon="qrcode-scan" 
        mode="elevated" 
        buttonColor='#3C5393' 
        contentStyle={{height:70,width:220}} 
        labelStyle={{fontSize:20, fontWeight:'bold'}} 
        textColor="white" 
        uppercase="true"
        onPress = {()=> navigation.navigate('Scanner',{'method':'mat-scan'})}
      >GET STARTED</Button>
      }
      <StatusBar style="dark" />
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      
      // justifyContent: 'center',
    },
    button: {
      shadowColor: "black",
      shadowOpacity: 0.8,
      shadowRadius: 2,
      shadowOffset: {
        height: 1,
        width: 1
      },
      textAlign: 'center',
      alignSelf: 'center',
      // justifyContent:"center",
      // alignItems: "center",
      borderRadius: 50,
      textAlignVertical: 'center',
      backgroundColor: "#3C5393",
      padding: 10,
      width: '70%',
      height: '10%',
      fontSize: 30,
      color:'white',
      fontWeight:'bold',
      marginTop:25
    },
    surface: {
      padding: 8,
      height: '25%',
      width: "100%",
      alignItems: 'center',
      justifyContent: 'center',
      marginTop:24, 
      marginBottom:15,
      backgroundColor: "#3C5393"
    },
    buttonContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      position: 'absolute',
      height: 50,
      width: 50,
      top: 10,
      right: 10,
      zIndex: 999,
      backgroundColor: 'white',
      borderRadius: 35,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 1,
      shadowRadius: 4,
      elevation: 5,
    },
  });
