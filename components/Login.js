import { StatusBar } from 'expo-status-bar';
import { useState,useCallback, useEffect } from 'react';
import { StyleSheet, View, Alert, Image, TouchableOpacity, TouchableWithoutFeedback, Keyboard, ActivityIndicator  } from 'react-native';
import { useTheme, Button, Surface, TextInput, Portal, Dialog, Text } from 'react-native-paper';
import HomeScreen from './HomeScreen'
// import Loader from './Loader'


export default function Login({navigation}) {
    const [employeeNumber, setEmployeeNumber] = useState("");
    const [password, setPassword] = useState("");
    const [loading, setLoading] = useState(false)
    const [dialogVisible, setDialogVisible] = useState(false);

  const theme = useTheme();
    
  const [isFocused, setIsFocused] = useState(false);
  const handleFocus = () => {
    setIsFocused(true);
  };

  const handleBlur = () => {

    setIsFocused(false);
  };
  const outFocus = () => {
    Keyboard.dismiss();
  };
  
  const login = () => {
    setLoading(true)
    // console.log(employeeNumber)
    fetch(`${global.backend}/api/login.php`,{
        method: 'POST',
        headers:{
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          'empNum': employeeNumber,
          'password': password,
          'secretKey': global.secretKey
        })
      })
        .then(response => response.json())
        .then(result => {
            console.log(result)
              
              if(result.message === "error" || result.message ==="unauthorized"){
                console.log(result)
                (async () => {
                
                  await setLoading(false)
                  setDialogVisible(true)
                //   setInvalid(true)
                })();
                return
              }

              if(result.message === "fail"){
                (async () => {
                  await setLoading(false)
                  setDialogVisible(true)
                //   setScannedEmployee(true)
                })();
                return
                // return console.log(result)
              }
              
              
              (async () => {
                // global.empNum = await result[0].EMP_NUM
                setLoading(false)
                global.credentials = result
                // global.userName = result[0].EMP_NUM;
                // global.password = result[0].password;
                console.log()
                navigation.navigate('Home',{'process': 'login'})
              })();
              console.log(global.userName)
              console.log(global.password)
              // return console.log(result[0].EMP_NUM)
              
          })
  }

  const handleUsernameChange = (text)=>{
    const cleanedValue = text.replace(/\D/g, '');
    setEmployeeNumber(cleanedValue)
  }
  const handlePasswordChange = (text)=>{
    setPassword(text)
  }
  useEffect(() => {
    // Set a timer to automatically dismiss the dialog after 3 seconds
    const timer = setTimeout(() => {
      setDialogVisible(false);
    }, 3000);

    // Clear the timer when the component is unmounted or dialogVisible changes to false
    return () => clearTimeout(timer);
  }, []);

  const loader = (
    <View style={{position:'absolute',height:'100%', width:'100%',zIndex:999999,backgroundColor: 'rgba(0, 0, 0, 0.5)',justifyContent:'center'}}>
        <View style={styles.loaderContainer}>
            <ActivityIndicator size="large" color={"#3C5393"}/>
            <Text style={styles.loaderText}>Processing...</Text>
        </View>
    </View>
  )

  return (
    <TouchableWithoutFeedback onPress={outFocus}>
    <View style={{backgroundColor:theme.colors.surface,flex:1,alignItems:'center'}}>
    {loading ? 
        loader
        :
    null}
    <Portal>
        <Dialog visible={dialogVisible} dismissable={true} onDismiss={() => setDialogVisible(false)} style={{ backgroundColor: '#fff' }}>
        <Dialog.Title>Login Failed</Dialog.Title>
        <Dialog.Content>
            <Text variant="bodyMedium">Username or password does not exist!</Text>
        </Dialog.Content>
            <Dialog.Actions style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
              <Button onPress={() => setDialogVisible(false)}>close</Button>
            </Dialog.Actions>
        </Dialog>
    </Portal>
        <View style={styles.surface}>
          <Image
            source={require('../assets/matech_logo_5.png')}
            style={{height:'50%', width:'45%'}}
          />
          
        </View>
        
        <TextInput
              maxLength={50}
              onFocus={handleFocus}
              onBlur={handleBlur}
              numberOfLines={1}
              editable={true}
              value={employeeNumber}
              style={{width:'80%', alignSelf:'center', backgroundColor:'#FFFFFF', marginTop:20}}
              mode="outlined"
              label="Employee number"
              placeholder="Enter employee number"
              keyboardType="numeric"
              theme={{
                colors: {
                    primary: isFocused ? '#414142' : '#414141',
                    underlineColor: 'transparent',
                }
              }}
              onChangeText={handleUsernameChange}
              // left={<TextInput.Icon icon="square-edit-outline" onPress={scanLotNumber}/>}
            />
            <TextInput
              maxLength={50}
              onFocus={handleFocus}
              onBlur={handleBlur}
              numberOfLines={1}
              editable={true}
              value={password}
              style={{width:'80%', alignSelf:'center', backgroundColor:'#FFFFFF', marginTop:20}}
              mode="outlined"
              label="Password"
              secureTextEntry={true}
              placeholder="Enter password"
            
              theme={{
                colors: {
                    primary: isFocused ? '#414142' : '#414141',
                    underlineColor: 'transparent',
                }
              }}
              onChangeText={handlePasswordChange}
              // left={<TextInput.Icon icon="square-edit-outline" onPress={scanLotNumber}/>}
            />
        
        <Button 
        style={{marginTop:30, width:'80%',alignItems:'center',justifyContent:'center',borderRadius:10}} 
        icon="account-key" 
        mode="elevated" 
        buttonColor='#3C5393' 
        contentStyle={{height:50,width:220}} 
        labelStyle={{fontSize:20, fontWeight:'bold'}} 
        textColor="white" 
        uppercase="true"
        onPress = {login}
      >Log-in</Button> 
      <StatusBar style="dark" />
    </View>
    </TouchableWithoutFeedback>
  )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      
      // justifyContent: 'center',
    },
    button: {
      shadowColor: "black",
      shadowOpacity: 0.8,
      shadowRadius: 2,
      shadowOffset: {
        height: 1,
        width: 1
      },
      textAlign: 'center',
      alignSelf: 'center',
      // justifyContent:"center",
      // alignItems: "center",
      borderRadius: 50,
      textAlignVertical: 'center',
      backgroundColor: "#3C5393",
      padding: 10,
      width: '70%',
      height: '10%',
      fontSize: 30,
      color:'white',
      fontWeight:'bold',
      marginTop:25
    },
    surface: {
      padding: 8,
      height: '25%',
      width: "100%",
      alignItems: 'center',
      justifyContent: 'center',
      marginTop:100, 
      
    //   marginBottom:15,
    //   backgroundColor: "#3C5393"
    },
    loaderContainer: {
        backgroundColor:'white',
    padding:20,
    width:'80%',
    height: '10%',
    // flex: 1,
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'flex-start',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    loaderText:{
        paddingLeft:'10%',
    },
    
  });
