import { View, ActivityIndicator } from 'react-native';
import {Dialog, Text } from 'react-native-paper';


const Loader = () => {
  
return (
    <View style={{
        zIndex: 999999,
        backgroundColor:'white',
        padding:20,
        width:'80%',
        height: '10%',
        // flex: 1,
        flexDirection: 'row',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'flex-start',
        shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,

            elevation: 5,
        }}>
        <ActivityIndicator size="large" color={"#3C5393"}/>
        <Text style={{paddingLeft:'10%'}}>Processing...</Text>
    </View>
    )
}  
export default Loader;