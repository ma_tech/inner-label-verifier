import { StatusBar } from 'expo-status-bar';
import { useState, useEffect, useRef } from 'react';
import { StyleSheet, View, ActivityIndicator, BackHandler, PanResponder, Dimensions, Animated  } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import QRIcon from 'react-native-vector-icons/MaterialIcons'
import React, { Component } from 'react';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { Button, Modal, Text, Provider, Dialog, Portal } from 'react-native-paper';
import Loader from './Loader'
import { Audio } from 'expo-av';


export default function Scanner({route, navigation}) {
  const [scanBoxWidth, setScanBoxWidth] = useState(200);
  const [scanBoxHeight, setScanBoxHeight] = useState(200);

  const scanBoxRef = useRef(null);
  const { width: screenWidth, height: screenHeight } = Dimensions.get('window');
  const [scannedData, setScannedData] = useState('');
  const [sound, setSound] = useState(null);
  const beepSound = require('../assets/sounds/beep-07a.wav');
  const [loading, setLoading] = useState(false)
  const myIcon = <Icon name="wifi" size={150} color="black" />;
  const qrIcon = <QRIcon name="qr-code-scanner" size={200} color="black" />
  const [name,setName] = useState('');
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);
  const [scannedEmployee, setScannedEmployee] = useState(false);
  const [invalid, setInvalid] = useState(false);
  const [failedSignal, setFailedSignal] = useState(false)
  const [duplicateSignal, setDuplicateSignal] = useState(false)
  const [uploadSuccessful, setUploadSuccessful] = useState(false)
  const [informationVisible, setInformationVisible] = useState(false);
  const [loaderForOK, setLoaderForOK] = useState(false)
  
  const hideInformationVisible = () => {
    // console.log("hide")
    setDuplicateSignal(false)
    setInformationVisible(false)
    setLoading(true)
  };
  
  const playSound = async () => {
    try {
      const { sound } = await Audio.Sound.createAsync(beepSound, { shouldPlay: true });
      setSound(sound);
    } catch (error) {
      console.error('Error playing audio:', error);
    }
  };



  const panResponder = PanResponder.create({
    onStartShouldSetPanResponder: () => true,
    onPanResponderMove: (event, gestureState) => {
      const { dx, dy } = gestureState;

      const newScanBoxWidth = Math.max(scanBoxWidth + dx, 0);
      const newScanBoxHeight = Math.max(scanBoxHeight + dy, 0);

      if (newScanBoxWidth <= screenWidth && newScanBoxHeight <= screenHeight) {
        setScanBoxWidth(newScanBoxWidth);
        setScanBoxHeight(newScanBoxHeight);
      }
    },
  });
// console.log(global.resultLength)
useEffect(() => {
        const backHandler = BackHandler.addEventListener('hardwareBackPress', () => true)
        return () => backHandler.remove()
      }, [])
  // useEffect(() => {
  //   (async () => {
  //     const { status } = await BarCodeScanner.requestPermissionsAsync();
  //     setHasPermission(status === 'granted');
  //   })();
  // }, [navigation]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      (async()=>{
        const { sound } = await Audio.Sound.createAsync(beepSound);
        setSound(sound);
      })()
      // do something - for example: reset states, ask for camera permission
      setLoading(false)
      setInvalid(false)
      // setSound()
      setScanned(false);
      setScannedEmployee(false);
      setHasPermission(false);
      setLoaderForOK(false);
      (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === "granted"); 
      })();
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return () => {
      unsubscribe();
      if (sound) {
        sound.unloadAsync();
      }
    };
  }, [navigation, sound]);


  const UploadData = ()=>{
    // console.log(global.qrData)
    
    // setLoader(true)
      fetch(`${global.backend}/api/upload.php`,{
          method: 'POST',
          headers:{
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            'qrData': global.qrData,
            'empNum': global.empNum,
            // 'boxAdditionalData': global.boxAdditionalData,
            'secretKey': global.secretKey
          })
        })
          .then(response => response.json())
          .then(result => {
            // console.log(result)
            if(result.message === "duplicate"){
              setScanned(true)
                setLoading(false)
                setDuplicateSignal(true)
                setInformationVisible(true)
                // console.log(result.message)
            }else if(result.message === "failed"){
              setScanned(true)
                setLoading(false)
                setFailedSignal(true)
                setInformationVisible(true)
                // console.log(result.message)
                console.log("too many characters for lot number")
            }else {
                uploadBarcodeDetails(result[0].ID)
                
                // console.log(result[0])
                
            }
          })
  }

  const uploadBarcodeDetails = (boxID)=>{
    fetch(`${global.backend}/api/upload-barcodes.php`,{
      method: 'POST',
      headers:{
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'boxID': boxID,
        'boxAdditionalData': global.boxAdditionalData.data,
        'secretKey': global.secretKey
      })
    })
      .then(response => response.json())
      .then(result => {
        // console.log(result)
        global.qrData = [];
        global.boxAdditionalData = {};
        global.empNum = "";
        setLoading(false)
        setUploadSuccessful(true)
        setInformationVisible(true)
      })
  }

  const checkEmployeeID = (empNum)=>{
    
    // setModalScanner(true)
    // return
    // setLoader(true)
      fetch(`${global.backend}/api/search-emp-num.php`,{
          method: 'POST',
          headers:{
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            'empNum': empNum,
            'secretKey': global.secretKey
          })
        })
          .then(response => response.json())
          .then(result => {
                
                if(result.message === "error" || result.message ==="unauthorized"){
                  // console.log(result)
                  (async () => {
                  
                    await setLoading(false)
                    setInvalid(true)
                  })();
                  return
                }

                if(result.message === "fail"){
                  (async () => {
                  
                    await setLoading(false)
                    setScannedEmployee(true)
                  })();
                  return
                  // return console.log(result)
                }
                
                
                (async () => {
                  global.empNum = await result[0].EMP_NUM
                  UploadData();
                })();
                
                // return console.log(result[0].EMP_NUM)
                
            }
          )
}
  
const handleBarCodeScanned = (event) => {
  const { type, data, bounds } = event;
  const { origin, size } = bounds;

  // Calculate the coordinates of the red box
  const redBoxLeft = (screenWidth - scanBoxWidth) / 2;
  const redBoxTop = (screenHeight - scanBoxHeight) / 2;
  const redBoxRight = redBoxLeft + scanBoxWidth;
  const redBoxBottom = redBoxTop + scanBoxHeight;

  // Check if the barcode is within the boundaries of the red box
  if (
    origin.x >= redBoxLeft &&
    origin.y >= redBoxTop &&
    origin.x + size.width <= redBoxRight &&
    origin.y + size.height <= redBoxBottom
  ) {
    // Barcode is within the red box, handle it
    playSound()
    setLoading(true);
    // console.log("data")
    // setInvalid(true)
    setScanned(true);
    // (async()=>{
    //   await playSound()
    //   setLoading(true);
    // })()
      // setScanned(true);
      if(route.params.method ==="lot-scan"){
        global.qrData[1].LOT_NUMBER = data
        setTimeout(() => {
          navigation.navigate('QRdetails')
        }, 10);
        return
      }else if(route.params.method ==="supplier-scan"){
        (async()=>{
          global.boxAdditionalData.data = await data
        // console.log(global.boxAdditionalData)
        setTimeout(() => {
          navigation.navigate('QRdetails')
        }, 10);
        })()
        return
      }else if(route.params.method ==="mat-scan"){
        // data = data.split(";");
        // (async () => {
        //   await fetchData(data)
        //   // setLoading(false)
        //   // navigation.navigate("QRdetails")
        // })();
        fetchData(data)
      }else if(route.params.method === "employee-scan"){
        checkEmployeeID(data)
        // (async () => {
        //   await checkEmployeeID(data)
        //   // setLoading(false)
        //   // navigation.navigate("QRdetails")
        // })();
  
      }
      
      // console.log(global.qrData)
      // navigation.navigate("QRdetails")
      // alert(`Bar code with type ${type} and data ${data} has been scanned!`);
    // Additional handling logic...
  }
   
 
    
  };
if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
}
if (hasPermission === false) {
    return <Text>No access to camera</Text>;
}

const fetchData =async (data)=>{
  return fetch(`${global.backend}/api/get-qr-details.php`,{
        method: 'POST',
        headers:{
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          'data': data,
          'secretKey': global.secretKey
        })
      })
        .then(response => response.json())
        .then(result => {
          console.log(result)
          setLoading(false)
          // return result
          if(result.length > 0){
            if(result.length > 1){
              global.firstLoadChecker = {"load": true}
            }else{
              global.firstLoadChecker = {"load": false}
            }
            // console.log(global.firstLoadChecker.load)
            global.resultLength = result.length
            
            global.qrData = result
            global.qrData.push({'LOT_NUMBER':''})
            navigation.navigate("QRdetails",{"reelScan":true})
            return
          }
          (async()=>{
            await setLoading(false)
            setScannedData(data)
            setInvalid(true);
            // console.error(error);
          })()
        })
        .catch(error => {
            (async()=>{
              await setLoading(false)
              setScannedData("Undefined format")
              setInvalid(true);
              // console.error(error);
            })()
        });
}


const loader = (
  <View style={{
    backgroundColor:'white',
    padding:20,
    width:'80%',
    height: '10%',
    // flex: 1,
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'flex-start'
  }}><ActivityIndicator size="large" color={"#3C5393"}/>
    <Text style={{paddingLeft:'10%'}}>Processing...</Text>
  </View>
)

const cancelBtn =()=>{
  setLoading(true)
  setTimeout(() => {
    if(route.params.method === "mat-scan"){
      navigation.navigate('Home')
    }else
    if(route.params.method ==="supplier-scan"){
      navigation.navigate('QRdetails')
    }else
    if(route.params.method ==="lot-scan"){
      navigation.navigate('QRdetails')
    }else
    if(route.params.method === "employee-scan"){
      navigation.navigate('QRdetails')
    }
    }, 1);
  
}
const frame = {
  x: (screenWidth - scanBoxWidth) / 2,
  y: (screenHeight - scanBoxHeight) / 2,
  width: scanBoxWidth,
  height: scanBoxHeight,
};
const allBarCodeTypes = Object.values(BarCodeScanner.Constants.BarCodeType);
// console.log(BarCodeScanner.Constants.BarCodeType)
  return (
    <View style={styles.container}>
      <BarCodeScanner
        onBarCodeScanned={(scanned || scannedEmployee || uploadSuccessful || invalid) ? undefined : handleBarCodeScanned}
        style={StyleSheet.absoluteFillObject}
        barCodeScannerProps={{
          // barCodeTypes: allBarCodeTypes, // Specify the types of barcodes you want to scan
          // onBarCodeScanned:  ()=>{
          //   console.log("scanning...")
          // },
          type: 'back', // Use the back camera for scanning
          // torchMode: 'off', // Turn off the torch mode
          // barCodeScannerSettings: {
          //   barCodeTypes: allBarCodeTypes, // Specify the types of barcodes you want to scan
          // },
          // Set the scanning area using the frame prop
          // Adjust the values based on the position and size of your red box
          frame: frame,
        }}
      >
        
        <Text variant='titleMedium' style={{alignSelf:'flex-start',paddingLeft:'5%', marginTop:'8%', fontWeight:'bold',color:'#3C5393'}}>QR/Barcode Reader</Text>
        
      </BarCodeScanner>
      <View
        {...panResponder.panHandlers}
        style={[
          styles.scanBox,
          { width: scanBoxWidth, height: scanBoxHeight },
        ]}
      />
      
      {invalid && <View><Button mode="elevated" onPress={() => 
        {
          (async()=>{
            setInvalid(false);
            setScanned(false);
            (async()=>{
              const { sound } = await Audio.Sound.createAsync(beepSound);
              setSound(sound);
            })()
          })()
        }
       } >Invalid QR code format</Button><View style={{backgroundColor:'white', padding:10, marginTop:10,borderRadius:5}}><Text style={{color:'red'}}>{scannedData}</Text></View></View>}
      {scannedEmployee && <Button mode="elevated" onPress={() => 
        {
          setScannedEmployee(false);
          setScanned(false);
        }
        } >Employee number does not exist!</Button>}
      
      {loading ? 
      (<Loader />) : 
      <Button
        style={{position:'absolute',bottom:30}}
        icon="close-circle"
        labelStyle={{fontSize: 60, marginRight: 0}}
        onPress={cancelBtn}
      ></Button>
      }

{
            duplicateSignal ?
            
                (<Dialog visible={informationVisible} onDismiss={hideInformationVisible} dismissable={false}>
                    <Dialog.Icon icon="alert" color='red' />
                    <Dialog.Content>
                        <Text variant="bodyMedium" style={{alignSelf:'center'}}>
                            {
                                duplicateSignal ?
                                "Duplicate Entry"
                                :
                                null
                            }
                        </Text>
                    </Dialog.Content>
                    <Button loading={loaderForOK} mode="Text" labelStyle={{color:'black'}} onPress={()=> 
                      {
                        (async () => {
                          await hideInformationVisible();
                          // await setLoaderForOK(true);
                          setTimeout(() => {
                            navigation.navigate('QRdetails');
                          }, 10);
                        })();
                        
                         
                        
                      }
                      }>OK</Button>
                </Dialog>)
            
            :
            null
        }
        {
            failedSignal ?
            
                (<Dialog visible={informationVisible} onDismiss={hideInformationVisible}>
                    <Dialog.Icon icon="alert" color='red' />
                    <Dialog.Content>
                        <Text variant="bodyMedium" style={{alignSelf:'center'}}>
                            {
                                failedSignal ?
                                "Upload Failed. Please contact the developer immediately!"
                                :
                                ""
                            }
                        </Text>
                    </Dialog.Content>
                    <Button labelStyle={{color:'black'}} onPress={()=> 
                      {
                        (async () => {
                          await hideInformationVisible();
                          // await setLoaderForOK(true);
                          setTimeout(() => {
                            navigation.navigate('QRdetails');
                          }, 10);
                        })();
                      }
                      }>OK</Button>
                </Dialog>)
            
            :
            ""
        }
        {
            uploadSuccessful ?
            
                (
                // <Provider>
                <Portal>
                    <Modal visible={informationVisible} onDismiss={hideInformationVisible} dismissable={false} contentContainerStyle={styles.successModalStyle}>
                        <Text variant="headlineMedium" style={{marginTop:20}}>Success!</Text>
                        <Text variant="bodyMedium" style={{marginTop:5}}>Data has been uploaded successfully!</Text>
                        <Button labelStyle={{color:'black', marginTop:20}} onPress={()=>navigation.navigate('Home')}>OK</Button>
                    </Modal>
                  </Portal>
                  // </Provider>  
                )
            
            :
            ""
        }
      
      
      <StatusBar style="dark" />
    </View>
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    
    justifyContent: 'center',
  },
  successModalStyle: {
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    justifyContent: 'flex-start',
    alignItems:'center',
    alignSelf:'center', 
    backgroundColor:'#fff', 
    width:'80%', 
    height:'20%',
    borderRadius:10
},
scanBox: {
  borderWidth: 2,
  borderColor: 'red',
  position: 'absolute',
  justifyContent: 'center',
  alignItems: 'center',
},
grayBackground: {
  ...StyleSheet.absoluteFillObject,
  backgroundColor: 'rgba(0, 0, 0, 0.5)', // Semi-transparent gray color
},
});
