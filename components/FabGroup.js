import * as React from 'react';
import { FAB, Portal, Provider } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
// import Spinner from './Spinner';

export default function FabGroup() {
    const [state, setState] = React.useState({ open: false });
    // const [spinner, setSpinner] = React.useState(false);

    const onStateChange = ({ open }) => setState({ open });
  
    const { open } = state;

    const navigation = useNavigation(); 
  
  return (
    // <Provider>
      <Portal>
        <FAB.Group
        fabStyle = {{backgroundColor:'#3C5393'}}
        color='#fff'
          open={open}
          visible
          icon={open ? 'close' : 'menu'}
          actions={[
            
            {
              icon: 'restart',
              label: 'Restart',
              onPress: () => {
                // <Spinner open={true} />
                // setSpinner(true)
                // global.qrData = [];
                // global.boxAdditionalData = [];
                navigation.navigate('QRdetails',{'modalType':'cancel'})
              },
            },
            {
              icon: 'cloud-upload',
              label: 'Upload data',
              onPress: () => {
                navigation.navigate('QRdetails',{'modalType':'upload'})
              },
            },
          ]}
          onStateChange={onStateChange}
          onPress={() => {
            if (open) {
              // do something if the speed dial is open
              console.log("ano to?")
            }
          }}
        />
        {/* <ConfirmationModal /> */}
        {/* {spinner ? <Spinner open={true}/> : null} */}
      </Portal>
    // </Provider>
  )
}


