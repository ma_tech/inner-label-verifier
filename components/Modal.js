import * as React from 'react';
import { StyleSheet, View, Alert } from 'react-native';
import { Button, Dialog, Portal, Text, Modal, Provider  } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import Loader from './Loader'


const ConfirmationModal = (props) => {
  const [visible, setVisible] = React.useState(false);

  const [informationVisible, setInformationVisible] = React.useState(false);

  const [loader, setLoader] = React.useState(false)
  const [failedSignal, setFailedSignal] = React.useState(false)
  const [duplicateSignal, setDuplicateSignal] = React.useState(false)
  const [uploadSuccessful, setUploadSuccessful] = React.useState(false)
  const [modalScanner, setModalScanner] = React.useState(false)
  const [empNum, setEmpNum] = React.useState();

  const hideModalScanner = () => setVisible(false);
  const hideDialog = () => setVisible(false);
  const hideInformationVisible = () => setInformationVisible(false);

  const navigation = useNavigation(); 
  
  React.useEffect(()=>{
    setVisible(props.open)
  },[props])
console.log(props.open)

const uploadScannedDetails = () => {
  fetch(`${global.backend}/api/upload.php`,{
    method: 'POST',
    headers:{
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      'qrData': global.qrData,
      'credentials': global.credentials,
      // 'boxAdditionalData': global.boxAdditionalData,
      'secretKey': global.secretKey
    })
  })
    .then(response => response.json())
    .then(result => {
      console.log(result)
      setLoader(false)
      if(result.message === "success"){
        global.qrData = [];
        global.boxAdditionalData = {};
        global.firstLoad = {};
        global.firsLoadChecker = {};
        // console.log("restart")
        setTimeout(() => {
          navigation.navigate('Home')
        }, 1);
      }
    })
}

  const yesAction = ()=>{
    setLoader(true)
    if (props.operation === "upload"){
        hideDialog()
        uploadScannedDetails()
       
            console.log("upload")
            // setTimeout(() => {
            // // navigation.navigate('Scanner', {'method':'employee-scan'})
            // }, 1);
        // navigation.navigate('Scanner', {'method':'employee-scan'})
    } else {
        // setLoader(true)
        global.qrData = [];
        global.boxAdditionalData = {};
        global.firstLoad = {};
        global.firsLoadChecker = {};
        console.log("restart")
        setTimeout(() => {
        navigation.navigate('Home')
        }, 1);
        
    }
  }

  return (
    <Portal>
        {
            loader ?
            (<View style={{zIndex:2,justifyContent:'center',alignItems:'center', flex:1, backgroundColor: 'rgba(0,0,0,0.5)'}}>
            <Loader />
        </View>)
        :
        ""
        }
        

      <Dialog visible={visible} onDismiss={hideDialog}>
      <Dialog.Icon icon="alert" color='red' />
      <Dialog.Title style={styles.title}>Are you sure you want to {props.operation}? </Dialog.Title>
      {
        props.content ? 
        (
        <Dialog.Content>
            <Text style={{alignSelf:'center'}} variant="bodyMedium">Please note that this will cancel everything and go back to start.</Text>
        </Dialog.Content>
        )
        :
        ""
      }
      {
        props.operation === "upload" ?
        (
            <Dialog.Content>
                <Text style={{alignSelf:'center'}} variant="bodyMedium">By clicking yes, data will be save and the screen will redirect to main screen.</Text>
            </Dialog.Content>
        )
        :
        null
      }
        
        <Dialog.Actions>
          <Button labelStyle={{color:'black'}} onPress={hideDialog}>Cancel</Button>
          <Button labelStyle={{color:'black'}} onPress={yesAction}>Yes</Button>
        </Dialog.Actions>
      </Dialog>
      {/* <Loader /> */}
    </Portal>
  );
};

const styles = StyleSheet.create({
    title: {
      textAlign: 'center',
    },
    successModalStyle: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        justifyContent: 'flex-start',
        alignItems:'center',
        alignSelf:'center', 
        backgroundColor:'#fff', 
        width:'80%', 
        height:'20%',
        borderRadius:10
    }
  })
  
export default ConfirmationModal;